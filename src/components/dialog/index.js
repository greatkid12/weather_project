import React, { useContext } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import PersonIcon from "@material-ui/icons/Person";
import AddIcon from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography";
import { blue } from "@material-ui/core/colors";
import { CountryContext } from "../../context/weather.context";

const emails = ["username@gmail.com", "user02@gmail.com"];
const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

function SimpleDialog(props) {
  const classes = useStyles();
  console.log("pro", props);
  const { onClose, selectedValue, open, data } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  const handleListItemClick = (value) => {
    onClose(value);
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <DialogTitle style={{justifyContent:'center',display:'flex',alignItems:'center'}} id="simple-dialog-title">Weather Details
      
       </DialogTitle>
      {data?.current.weather_icons?.map((item) => {
            return <img style={{alignSelf:'center'}} width={50} height={50} src={item} />;
          })}
      <List>
        <ListItem >
          
          <ListItem>
            <ListItemText>Temperature:{data?.current.temperature}</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Wind speed:{data?.current.wind_speed}</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Precip:{data?.current.precip}</ListItemText>
          </ListItem>
        </ListItem>
      </List>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  selectedValue: PropTypes.string.isRequired,
};

export default function SimpleDialogDemo(props) {
  console.log("props", props);
  const [open, setOpen] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState(emails[1]);
  const data = useContext(CountryContext);
  const onClick = async (name) => {
    data.getWeather(name, (res) => {
      console.log("res", res);
    });
  };

  const handleClickOpen = () => {
    setOpen(true);
    onClick(props.value.name);
  };

  const handleClose = (value) => {
    setOpen(false);
    setSelectedValue(value);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        See Weather
      </Button>
      <SimpleDialog
        data={data.weather}
        selectedValue={selectedValue}
        open={open}
        onClose={handleClose}
      />
    </div>
  );
}
