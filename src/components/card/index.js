import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { CountryContext } from "../../context/weather.context";
import { Grid, Button, List, ListItem, ListItemText } from "@material-ui/core";
import SimpleDialogDemo from "../dialog";

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: 100,
    width: 300,
    marginTop: 100,
    marginRight: 50,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const data = useContext(CountryContext);
  console.log("data", data);

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const onClick = async (name) => {
    data.getWeather(name.name, (res) => {
      console.log("res", res);
    });
  };
  return (
    <div>
      <Grid item xs={12}>
        <Grid container justifyContent="center" spacing={7}>
          {data.country?.map((value) => (
            <Card className={classes.root}>
              <CardHeader
                avatar={
                  <Avatar aria-label="recipe" className={classes.avatar}>
                    C
                  </Avatar>
                }
                action={
                  <IconButton aria-label="settings">
                    <MoreVertIcon />
                  </IconButton>
                }
                title={value.name}
                subheader={value.capital}
              />
              <CardMedia
                className={classes.media}
                image={value.flag}
                title="Paella dish"
              />
              <CardContent>
                <List>
                  <ListItem button>
                    <ListItemText primary="Latitude / Longitude" />
                    {value.latlng.map((item)=><ListItemText primary={item} />) }

                  </ListItem>
                  <ListItem button>
                    <ListItemText primary="Population" />
                    <ListItemText primary={value.population} />

                  </ListItem>
                 
                </List>
              </CardContent>
              <CardActions disableSpacing>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    marginLeft: "auto",
                    marginRight: "auto",
                  }}
                >
                  <SimpleDialogDemo value={value} />
                </div>
              </CardActions>
            </Card>
          ))}
        </Grid>
      </Grid>
    </div>
  );

  // )} )
}
