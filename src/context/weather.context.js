import { createContext } from "react";
import { useReducer } from "react";
import axios from "axios";

const intialState = {
  country: null,
  weather:null
};

//types
const REDUCER_TYPES = {
  GET_COUNTRY: "GET_COUNTRY",
  GET_WEATHER: "GET_WEATHER",

};

//reducers
const reducer = (state, action) => {
  // console.log('action',action)
  switch (action.type) {
    case REDUCER_TYPES.GET_COUNTRY:
      return { ...state, country: action.response };
      case REDUCER_TYPES.GET_WEATHER:
      return { ...state, weather: action.response };
    default:
      return state;
  }
};

export const CountryContext = createContext();

export const CountryContextProvider = ({ children }) => {
  const [setCountry, dispatch] = useReducer(reducer, intialState);

  const getCountry  = (value) => {

    console.log('context')
    return axios.get( `http://restcountries.eu/rest/v2/name/${value}`).then((response)=>{
console.log('res',response)
        dispatch({
          type: REDUCER_TYPES.GET_COUNTRY,
          response:response.data
        });
    })
  };
  const getWeather  = (country,onSuccess) => {

    console.log('context')
    return axios.get( `http://api.weatherstack.com/current?access_key=66b7cfeae9698ee5b4b4efbe72d2201f&query=${country}`).then((response)=>{
console.log('res',response)
        dispatch({
          type: REDUCER_TYPES.GET_WEATHER,
          response:response.data
        });
        onSuccess(response.data)
    })
  };
  
  return (
    <CountryContext.Provider value={{ ...setCountry, getCountry,getWeather }}>
      {children}
    </CountryContext.Provider>
  );
};
